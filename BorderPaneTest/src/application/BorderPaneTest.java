package application;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
//import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
//import javafx.scene.control.*;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.BorderPane;
import javafx.collections.FXCollections;
import javafx.scene.control.TextField;
import javafx.scene.control.ListView;
import javafx.geometry.*;

public class BorderPaneTest extends Application {

    @Override
    public void start(Stage primaryStage) {
    
    	
    		BorderPaneTest borderpane = new BorderPaneTest();
    		ToolBar toolbar = new ToolBar(); 
    		//HBox statusbar = new HBox();
    		//Node appContent = new AppContentNode();
    		Button button= new Button("Add"); 
    		BorderPane.setAlignment(button,Pos.CENTER_RIGHT);
    		Button button2 = new Button("Subtract");
    		BorderPane.setAlignment(button2,Pos.CENTER_RIGHT);
    		//borderPane.setTop(toolbar);
    		//borderPane.setCenter(button);
    		//borderPane.setBottom(statusbar);
    		BorderPane borderPane = new BorderPane(button, button2, null, null, null);
    		
 Scene scene = new Scene(borderPane, 300, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
 public static void main(String[] args) {
        launch(args);
    }
}