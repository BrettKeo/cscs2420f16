package Crackulator;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.scene.text.Text;

public class Crackulator extends Application {
	private Stage primaryStage;
	private GridPane btnGrid;
	private Text display;
	Integer num0ne = 0;
	Integer num2wo = 0;
	
    @Override
    public void start(Stage primaryStage) {
    	this.primaryStage = primaryStage;
    	
        StackPane root = new StackPane();
        
        display = new Text("Hit CLR you Crack Head");
        
        buttonJen();
        
        root.getChildren().addAll(btnGrid, display);

 Scene scene = new Scene(root, 300, 250);

        this.primaryStage.setTitle("CRACKULATOR");
        this.primaryStage.setScene(scene);
        this.primaryStage.show();
    }
    
    public int mat2arr(int x, int y, int col) {
    	return (y * col) + x;
    }
    
    private void buttonJen() {
    	String[] actions = {
    			"0", "1", "2", "3",
    			"4", "5", "6", "7",
    			"8", "9", "+", "-",
    			"*", "/", "%", ".",
    			"(", ")", "=", "CLR"
    	};
    	
    	btnGrid = new GridPane();
    	
    	
    	for (int y = 0; y < 5; y++)
    		for (int x = 0; x < 4; x++) {
    			Button btn = new Button();
    			btn.setOnAction(new EventHandler<ActionEvent>() {
    				@Override
    				public void handle(ActionEvent event) {
    					String action = ((Button)event.getSource()).getText();
    					
    					switch (action) {
    					case "0":
    					case "1":
    					case "2":
    					case "3":
    					case "4":
    					case "5":
    					case "6":
    					case "7":
    					case "8":
    					case "9":
    					case ".":	
    						if (display.getText().equals("+")){
    						display.setText("");
    						}
    						display.setText(action.concat(display.getText()));
    						break;
    						
    					case "+":
    						num0ne=Integer.parseInt(display.getText());
    						display.setText("+");
    						break;
    						
    					case "-":
    						num2wo=Integer.parseInt(display.getText());
    						display.setText("-");
    						break;
    					
    					case "=":
    						Integer equa1=Integer.parseInt(display.getText());
    						Integer grandTota1 = (num0ne + equa1);
    						display.setText(grandTota1.toString());
    						// Add number
    						break;
    					case "CLR":
    						display.setText("");
    						break;
    					default:
    						System.err.println("Invalid operation!");
    					}
    						
    				}
    			});
    			btn.setText(actions[mat2arr(x, y, 4)]);
    			btnGrid.add(btn, x, y);
    		}
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}
