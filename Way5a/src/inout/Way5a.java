package inout;

import java.io.*;
import java.net.URL;
import java.net.MalformedURLException;

public class Way5a {
	public static void main(String[] args){
		InputStreamReader isr = null;
		URL url = null;
		try{
			url = new URL("http://google.com");
		} catch(MalformedURLException ex){ }
		StringBuilder sb = new StringBuilder( );
		try{
			isr = new InputStreamReader( url.openStream() );
		}	catch(IOException ex){ }
		int in=0;
		try{
			while((in = isr.read())!=-1){
				sb.append((char)in);
			}
			System.out.println(sb.toString());
		} catch(IOException ex){ } 
	}
}