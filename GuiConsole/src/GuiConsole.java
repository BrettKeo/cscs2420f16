import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.Dimension;


abstract class GuiConsole extends JPanel implements KeyListener{
JTextArea display;
JScrollPane scroll;
JTextField keyboard;

	public GuiConsole(){
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.display = new JTextArea();
		this.display.setFont(new Font("Courier New", Font.PLAIN, 12));
		this.scroll = JScrollPanel(display);
		this.scroll.setPreferredSize(new Dimension(400,200));
		this.add(scroll);
		this.keyboard = JTextField();
		this.keyboard.addKeyListener(this);
		this.keyboard.setPreferredSize(new Dimension(400,30));
		this.add(keyboard);
		JFrame frame= new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(this);
		frame.pack();				   							
		frame.setVisible(true);
			
	}
	
	private JTextField JTextField() {
		// TODO Auto-generated method stub
		return null;
	}

	private JScrollPane JScrollPanel(JTextArea display2) {
		// TODO Auto-generated method stub
		return null;
	}

	abstract void act();
	public void keyPressed (KeyEvent event){
		if (event.getKeyCode()==KeyEvent.VK_ENTER) act();
		
	}
	public void keyReleased(KeyEvent event){}
	public void keyTyped(KeyEvent event){}
}

