import java.io.*;
import java.util.Collections;
import java.util.List;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.scene.text.Text;


public class Final extends Application{
	private Stage primaryStage;
	private Button click;
	private Text earth;
	private AnchorPane window;
	private List<String> languages; 
	
	
	@Override
	public void start(Stage primaryStage){
		this.primaryStage = primaryStage;
		
		earth = new Text ("Hi");
		window = new AnchorPane();
		Scene scene=new Scene(window, 300, 200);
		
		this.languages=ArrayList<String>();
		
		this.primaryStage.setTitle("Hello World");
		this.primaryStage.setScene(scene);
		this.primaryStage.show();
		
		click=new Button();
		click.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event){
				Collections.shuffle(languages);
				earth.setText(languages.get(0));
			}		
			});
		
		FileReader fr =null;
		BufferedReader br=null;
		
		//StringBuilder sb = new StringBuilder();
		//String str;
		try{
			fr=new FileReader("./Finalworld.txt");
			br=new BufferedReader(fr);
		}catch(FileNotFoundException ex){}
		
		try{
			for(String line = br.readLine(); line!= null;){
				this.languages.add(line);
			}
			//fr = new FileReader("./Finalworld.txt");
			//str = br.readLine();
			//sb.append(str);
			earth.setText(languages.get(0));
		}catch(IOException ex){}
			
		
		
		primaryStage.setResizable(false);
		click.setText("Click");
		window.getChildren().addAll(earth, click);
		AnchorPane.setTopAnchor(earth, 125.0);
		AnchorPane.setLeftAnchor(earth, 100.0);
		AnchorPane.setBottomAnchor(click, 10.0);
		AnchorPane.setRightAnchor(click,75.0);
		AnchorPane.setLeftAnchor(click, 75.0);
		
	
		}	
	
		public static void main(String[]args){
		launch(args);
		
	}
	
	}


