package inout;

import java.io.*;

public class Way2a {
	public static void main(String[] args){
		FileReader fis=null;
		StringBuilder sb = new StringBuilder( );
		try{
			fis = new FileReader("ascii.txt");
		}	catch(FileNotFoundException ex){ }
		int in=0;
		try{
			while((in = fis.read())!=-1){
				sb.append((char)in);
			}
			System.out.println(sb.toString());
		} catch(IOException ex){ } 
	}
}